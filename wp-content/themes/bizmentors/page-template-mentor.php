<?php
/*
Template Name: Mentors
*/

get_header();

$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() ); ?>

<div id="main-content">

<?php if ( ! $is_page_builder_used ) : ?>

	<div class="container">
		<div id="content-area" class="clearfix">
			<div id="left-area">

<?php endif; ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php if ( ! $is_page_builder_used ) : ?>

					<!--<h1 class="main_title"><?php the_title(); ?></h1>-->
				<?php
					$thumb = '';

					$width = (int) apply_filters( 'et_pb_index_blog_image_width', 1080 );

					$height = (int) apply_filters( 'et_pb_index_blog_image_height', 675 );
					$classtext = 'et_featured_image';
					$titletext = get_the_title();
					$thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
					$thumb = $thumbnail["thumb"];

					if ( 'on' === et_get_option( 'divi_page_thumbnails', 'false' ) && '' !== $thumb )
						print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height );
				?>

				<?php endif; ?>

					<div class="entry-content">
					<?php
						the_content();

						if ( ! $is_page_builder_used )
							wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
					?>
					</div> <!-- .entry-content -->

				<?php
					if ( ! $is_page_builder_used && comments_open() && 'on' === et_get_option( 'divi_show_pagescomments', 'false' ) ) comments_template( '', true );
				?>

				</article> <!-- .et_pb_post -->

			<?php endwhile; ?>

<?php if ( ! $is_page_builder_used ) : ?>

			</div> <!-- #left-area -->

			<?php get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->

<?php endif; ?>

</div> <!-- #main-content -->


<?php 
	$args = array(
		"post_type" => "mentor"
		);

	$query = new WP_Query($args);?>

<div class="mentor-grid">

	<?php if($query->have_posts()): while($query->have_posts()): $query->the_post(); ?>

		<div class="mentor">
			<a href = "<?php the_permalink(); ?>"><?php the_post_thumbnail("thumbnail"); ?><h1 class="main_title"><?php the_title(); ?></h1></a>
		</div>

	<?php endwhile; endif; wp_reset_postdata(); ?>

</div>


<div id="pre-footer">
	<div id="top">
		<div id="top-left">
		<image src="http://bizmentorsgalway.staging.educatedmachine.com/wp-content/uploads/2017/11/Connect-biz-mentors1.png">
		<p style="color: white;">Connect with Bizmentors<br>
		Email | info@bizmentors.ie</p>
		</div>

		<div id="top-right">
		<image src="http://bizmentorsgalway.staging.educatedmachine.com/wp-content/uploads/2017/11/Sign-up.png">
		<p style="color: white;">Latest news &amp; updates<br>
		Latest on the blog</p>
		</div>
	</div>

	<div id="bottom">
		<div id="bottom-left">
		<image src="http://bizmentorsgalway.staging.educatedmachine.com/wp-content/uploads/2017/11/Mentorship1.png">
		<p style="color: white;">Apply for Business Mentoring<br>
		Find out more</p>
		</div>

		<div id="bottom-right">
		<image src="http://bizmentorsgalway.staging.educatedmachine.com/wp-content/uploads/2017/11/Become-a-Mentor.png">
		<p style="color: white;">Apply to become a Mentor<br>
		Find out more</p>
		</div>
	</div>
</div>

<?php get_footer(); ?>

