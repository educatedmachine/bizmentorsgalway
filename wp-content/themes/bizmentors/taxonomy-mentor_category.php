<?php

get_header();?>



<div class="mentor-grid">

		<?php if(have_posts()): while(have_posts()): the_post(); ?>

		<div class="mentor">
			<a href = "<?php the_permalink(); ?>"><?php the_post_thumbnail("thumbnail"); ?><h1 class="main_title"><?php the_title(); ?></h1></a>
		</div>

	<?php endwhile; endif; wp_reset_postdata(); ?>


</div>

<div id="pre-footer">
	<div id="top">
		<div id="top-left">
		<image src="http://bizmentorsgalway.staging.educatedmachine.com/wp-content/uploads/2017/11/Connect-biz-mentors1.png">
		<p style="color: white;">Connect with Bizmentors<br>
		Email | info@bizmentors.ie</p>
		</div>

		<div id="top-right">
		<image src="http://bizmentorsgalway.staging.educatedmachine.com/wp-content/uploads/2017/11/Sign-up.png">
		<p style="color: white;">Latest news &amp; updates<br>
		Latest on the blog</p>
		</div>
	</div>

	<div id="bottom">
		<div id="bottom-left">
		<image src="http://bizmentorsgalway.staging.educatedmachine.com/wp-content/uploads/2017/11/Mentorship1.png">
		<p style="color: white;">Apply for Business Mentoring<br>
		Find out more</p>
		</div>

		<div id="bottom-right">
		<image src="http://bizmentorsgalway.staging.educatedmachine.com/wp-content/uploads/2017/11/Become-a-Mentor.png">
		<p style="color: white;">Apply to become a Mentor<br>
		Find out more</p>
		</div>
	</div>
</div>

<?php get_footer(); ?>

